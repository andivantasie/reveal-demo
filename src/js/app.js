
import Reveal from 'reveal.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Hightlight from 'reveal.js/plugin/highlight/highlight.esm.js';
import Notes from 'reveal.js/plugin/notes/notes.esm';
import Math from 'reveal.js/plugin/math/math.esm';

let deck = new Reveal({
    plugins: [
        Markdown,
        Hightlight,
        Math,
        Notes
    ]
});
deck.initialize({

    // Add the current slide number to the URL hash so that reloading the
    // page/copying the URL will return you to the same slide
    hash: true,

    // Transition style
    transition: 'zoom', // none/fade/slide/convex/concave/zoom

    math: {
        mathjax: 'https://cdn.jsdelivr.net/gh/mathjax/mathjax@2.7.8/MathJax.js',
        config: 'TeX-AMS_HTML-full',
        displayAlign: 'left',
        TeX: {
            equationNumbers: { autoNumber: "AMS"},
            Macros: { RR: "{\\bf R}" }
        }
    }
});
