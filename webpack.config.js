// import plugins
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {

    let isProduction = (argv.mode === 'production');
    let config = {

        // absolute path to the base directory
        context: path.resolve(__dirname, "src"),

        // entry files to compile (relative to the base dir)
        entry: [
            "./js/app.js",
            "./scss/app.scss",
        ],

        // enable development source maps
        // * will be overwritten by 'source-maps' in production mode
        devtool: "inline-source-map",

        // path to store compiled JS bundle
        output: {
            // bundle relative name
            filename: "js/app.js",
            // base build directory
            path: path.resolve(__dirname, "dist")
        },

        // plugins configurations
        plugins: [
            // save compiled SCSS into separated CSS file
            new MiniCssExtractPlugin({
                filename: "css/style.css"
            }),
        ],

        // production mode optimization
        optimization: {
            minimizer: [
                // CSS optimizer
                new OptimizeCSSAssetsPlugin(),
                // JS optimizer by default
                new TerserPlugin(),
            ],
        },

        // custom loaders configuration
        module: {
            rules: [
                // styles loader
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        "sass-loader"
                    ],
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                                outputPath: 'fonts/'
                            }
                        }
                    ]
                },
            ]
        },

    };

    // PRODUCTION ONLY configuration
    if (isProduction) {
        config.plugins.push(
            // clean 'dist' directory
            new CleanWebpackPlugin()
        );
    }

    return config;
};
